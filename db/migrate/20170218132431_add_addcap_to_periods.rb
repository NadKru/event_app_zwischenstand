class AddAddcapToPeriods < ActiveRecord::Migration
  def change
    add_column :periods, :addcaps, :integer
  end
end