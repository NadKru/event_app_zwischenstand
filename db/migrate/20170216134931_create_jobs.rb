class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name
      t.float :processing_time
      t.integer :ressources_demand

      t.timestamps null: false
    end
  end
end
