class CreatePeriods < ActiveRecord::Migration
  def change
    if (!ActiveRecord::Base.connection.tables.include?("periods"))
    create_table :periods do |t|
      t.string :name
      t.integer :addcap

      t.timestamps null: false
    end
  end
end
end
