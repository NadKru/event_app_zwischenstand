User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(name:  "Stefan Helber",
             email: "stefan.helber@prod.uni-hannover.de",
             password:              "geheim",
             password_confirmation: "geheim",
             admin: true)

User.create!(name: "Karl-Heinz Schwarz", email: "schwarz@gmail.com", password: "geheim", password_confirmation: "geheim")
User.create!(name: "Susi Rose", email: "rose@gmail.com", password: "geheim", password_confirmation: "geheim")
User.create!(name: "Jochen Flieger", email: "flieger@tui.com", password: "geheim", password_confirmation: "geheim")


99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

  users = User.order(:created_at).take(6)
  50.times do
    content = Faker::Lorem.sentence(5)
    users.each { |user| user.microposts.create!(content: content) }
    end


(0..15).each do |n|
  name = "t#{n}"
  Period.create!(name: name)
end



