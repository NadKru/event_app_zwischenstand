class EventJobPeriodAssociation < ActiveRecord::Base
  belongs_to :events
  belongs_to :jobs
  belongs_to :periods
end
