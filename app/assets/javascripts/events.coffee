# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  $('#events').dataTable({
    sPaginationType: "full_numbers"
    bJQueryUI: true
    "language": {
      "lenghtMenu": "Einträge pro Seite: _MENU_",
      "sSearch": "Suche:",
      "oPaginate": {
        "sPrevious": "vorherige Seite"
        "sNext": "nächste Seite"
      }
    }
    "dom": '<"top"i>rt<"bottom"flp><"clear">',
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false
  })


