set p /p1, p2, p3/;


set i / i1*i8/;

* Achtung: Erste Periode/erster Zeitpunkt muss 0 sein

set t /t0*t200/;


VN(h,i)=no;

* Achtung: Topologische Sortierung erforderlich

VN('i1','i2')=yes;
VN('i2','i3')=yes;
VN('i3','i4')=yes;
VN('i3','i5')=yes;
VN('i4','i6')=yes;
VN('i5','i6')=yes;
VN('i6','i7')=yes;
VN('i7','i8')=yes;



parameter
        d(i) /
         i1      0
         i2      21
         i3      7
         i4      7
         i5      7
         i6      14
         i7      1
         i8      0/;


parameter
Deadline(p) /
p1       50
p2       100
p3       80/;


k(i)=0;

k('i1')=0;
k('i2')=3;
k('i3')=1;
k('i4')=1;
k('i5')=1;
k('i6')=3;
k('i7')=3;
k('i8')=0;


KP=5;

oc=200;
