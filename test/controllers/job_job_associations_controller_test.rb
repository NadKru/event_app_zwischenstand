require 'test_helper'

class JobJobAssociationsControllerTest < ActionController::TestCase
  setup do
    @job_job_association = job_job_associations(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:job_job_associations)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create job_job_association" do
    assert_difference('JobJobAssociation.count') do
      post :create, job_job_association: { predecessor_id: @job_job_association.predecessor_id, successor_id: @job_job_association.successor_id }
    end

    assert_redirected_to job_job_association_path(assigns(:job_job_association))
  end

  test "should show job_job_association" do
    get :show, id: @job_job_association
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @job_job_association
    assert_response :success
  end

  test "should update job_job_association" do
    patch :update, id: @job_job_association, job_job_association: { predecessor_id: @job_job_association.predecessor_id, successor_id: @job_job_association.successor_id }
    assert_redirected_to job_job_association_path(assigns(:job_job_association))
  end

  test "should destroy job_job_association" do
    assert_difference('JobJobAssociation.count', -1) do
      delete :destroy, id: @job_job_association
    end

    assert_redirected_to job_job_associations_path
  end
end
