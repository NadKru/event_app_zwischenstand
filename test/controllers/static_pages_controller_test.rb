require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  def setup
    @base_title = "| EvenTraum"
  end

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Startseite #{@base_title}"
  end

  test "should get offer" do
    get :offer
    assert_response :success
    assert_select "title", "Angebot #{@base_title}"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "Über uns #{@base_title}"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Kontakt #{@base_title}"
  end

  test "should get impressum" do
    get :impressum
    assert_response :success
    assert_select "title", "Impressum #{@base_title}"
  end

  end

